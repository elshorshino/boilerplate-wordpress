<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

if(file_exists(dirname(__FILE__).'/local-config.php')):
	include(dirname(__FILE__).'/local-config.php');
	define('WP_ENV', 'local');
elseif(file_exists(dirname(__FILE__).'/develop-config.php')):
	include(dirname(__FILE__).'/develop-config.php');
	define('WP_ENV', 'develop');
else:
	define('WP_ENV', 'production');
	define('DB_NAME', 'dbname');
	define('DB_USER', 'username');
	define('DB_PASSWORD', 'password');
	define('DB_HOST', 'localhost');
	define('DB_CHARSET', 'utf8');
	define('DB_COLLATE', '');
endif;

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^1u]5G0>Jc.%Yb9s.RK5Q>nzk.=P.=.^V**vp;(P>jvN5rrimj+W{,3hb2]pA8zM');
define('SECURE_AUTH_KEY',  'A1j%.jWr85i,K-CK~|PBXC-6|%:n-DQch@W6nx p gk||]ug|++1}s;GJ+Fq~. A');
define('LOGGED_IN_KEY',    '-~xS-HJ>8|HGRueupU.|z|H4:45P_pZ|-_9D<f{d%tpW98w%w,&:7|9L=M|,UI3u');
define('NONCE_KEY',        'Y62l}<m^vNsb` d |-RGObKlUG&]lMjp->+RO?Ct!i J;11M)lg7kGhEW8QIY{bm');
define('AUTH_SALT',        'Jt}dU(,iB6=]F?:S b:IL7NZc7pXwZO+M;0yo>ms4F<eUZ+.>DU,Mckm+5}SgA++');
define('SECURE_AUTH_SALT', '3v{G6w=J9cE=q=.#kG$_GOg0K1}Au,=pXpcup,{HHi*3Xlr7@P{)f]~ADRfh9q-f');
define('LOGGED_IN_SALT',   'W+AnM?nS*D,+OACO6||,)[_7:&57E[?|8(:S_ow-ergtzp]~8z+!P8B5Bt_*Y,b[');
define('NONCE_SALT',       'oaF-KxC~]2fi$Jt:SZyf3(_+S?q9&Z&#*-`.>,Y/,6FN%Z@r(iqlI~H-%7&X;XS-');

$table_prefix  = 'boilerwp_';

define('WP_DEBUG', true);

/* Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) define('ABSPATH', dirname(__FILE__) . '/');

/* Change location of wp-content folder */
define('WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/content');
define('WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/content');

/* Limit post revisions */
define('WP_POST_REVISIONS', 10);

/* Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
