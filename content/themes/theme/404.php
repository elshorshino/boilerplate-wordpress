<?php the_post();
$body_classes = array("four-oh-four");

require("inc/header.php"); ?>
	<main class="main full">
		<div class="content">
			<div class="sleeve">
				<h1>404!</h1>
				<p>Oops - Wrong Turn!</p>
			</div>
		</div>
	</main><?php
require("inc/footer.php"); ?>
</body>
</html>