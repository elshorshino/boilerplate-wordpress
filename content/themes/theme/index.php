<?php the_post();
require("inc/header.php"); ?>
	<main class="main full">
		<?php the_content(); ?>
	</main><?php
require("inc/footer.php"); ?>
</body>
</html>