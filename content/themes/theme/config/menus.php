<?php
# Setup Navigation positions
function ss_menu_setup() {
	register_nav_menus(array(
		'main_menu' => "Main Navigation",
	));
}

add_action('init', 'ss_menu_setup');

# Custom Walker, prints out much tidier nicer code for navigation, based on original walker, cleaner.
class ss_Nav_Menu extends Walker_Nav_Menu {
	var $tree_type = array('post_type', 'taxonomy', 'custom');
	var $db_fields = array('parent' => 'menu_item_parent', 'id' => 'db_id');

	function start_lvl(&$output, $depth = 0, $args = array()) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul>\n";
	}

	function end_lvl(&$output, $depth = 0, $args = array()) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}

	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
		global $wp_query, $counter, $post;
		$indent = ($depth) ? str_repeat("\t", $depth) : '';

		$attributes = !empty($item->target)	? ' target="' . esc_attr($item->target) . '"' : '';
		$attributes .= !empty($item->url)	? ' href="'   . esc_attr($item->url) . '"' : '';

		@$class_names .= str_replace(array("current-menu-item", "current-menu-ancestor", "current_page_parent"), array("current", "current-parent", "current-page-parent"), $item->classes[4]);

		if($item->attr_title != '' && $item->attr_title == $post->post_type): ($class_names) ? $class_names .= " current-parent" : $class_names = "current-parent"; endif;
		if($item->attr_title == "special" && isset($output) && $class_names == "current") $output = str_replace("current", "", $output);
		if(is_404()) $output = str_replace("current", "", $output);

		($class_names) ? $class_names = ' class="nav-item ' . esc_attr( $class_names ) . '"' : $class_names = ' class="nav-item"';

		$output .= $indent . '<li' . @$value . @$class_names .'>';

		$item_output = @$args->before;
		$item_output .= '<a'. $attributes .'><span>';
		$item_output .= @$args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . @$args->link_after;
		$item_output .= '</span></a>';
		$item_output .= @$args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		$counter++;
	}

	function end_el(&$output, $item, $depth = 0, $args = array()) {
		$output .= "</li>";
	}
}

?>