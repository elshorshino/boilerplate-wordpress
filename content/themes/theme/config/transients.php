<?php

# Transient Functions.
function ss_trans_nav($nav, $expiry = DAY_IN_SECONDS) {
	# If we can't get the transient requested, create and set it.

	if(false === ($trans_nav = get_transient("tknav_".$nav))):
		# Custom walker for the navigation.
		$walker = new ss_Nav_Menu();

		# Grab the navigation using wp_nav_menu.
		$trans_nav = wp_nav_menu(array(
			'theme_location' 	=> $nav,
			'container_class' 	=> $nav . '-sleeve',
			'menu_class' 		=> '',
			'walker' 			=> $walker,
			'items_wrap' 		=> '<ul>%3$s</ul>',
			'echo'				=> false
		));

		set_transient("tknav_".$nav, $trans_nav, $expiry);
	endif;

	return $trans_nav;
}

# Transient Management, Deletion
add_action('wp_update_nav_menu', function() {
	delete_transient('tknav_main_menu');
});

?>