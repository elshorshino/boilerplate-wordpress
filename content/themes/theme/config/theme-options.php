<?php

function ss_theme_customizer($wp_customize) {
	 // Example fields
	# Custom Logo Section
	$wp_customize->add_section('ss_logo_section', array(
		'title'			=> "Logo",
		'priority'		=> 30,
		'description'	=> 'Upload a logo to replace the default logo.',
	));

	# Logo Control
	$wp_customize->add_setting('ss_logo');
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'ss_logo', array(
		'label'		=> 'Logo',
		'section'	=> 'ss_logo_section',
		'settings'	=> 'ss_logo',
	)));

	# Logo Alt Text
	$wp_customize->add_setting('ss_logo_alt');
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'ss_logo_alt', array(
		'label'		=> 'Logo Alt Text',
		'section'	=> 'ss_logo_section',
		'settings'	=> 'ss_logo_alt',
	)));

	# Footer Section
	$wp_customize->add_section('ss_footer_section', array(
		'title'			=> "Footer",
		'priority'		=> 31,
		'description'	=> 'Manage the footer area',
	));

	# Footer Text
	$wp_customize->add_setting('ss_footer_text');
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'ss_footer_text', array(
		'label'		=> 'Footer Text',
		'section'	=> 'ss_footer_section',
		'settings'	=> 'ss_footer_text',
	))); 

	# Social Media Options
	$wp_customize->add_section('ss_social_media_section', array(
		'title'			=> "Social Media",
		'priority'		=> 32,
		'description'	=> 'Manage your global social media links',
	));

	# Social Media Links
	$wp_customize->add_setting('ss_social_media_twitter');
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'ss_social_media_twitter', array(
		'label'		=> 'Twitter',
		'section'	=> 'ss_social_media_section',
		'settings'	=> 'ss_social_media_twitter',
	)));

	$wp_customize->add_setting('ss_social_media_facebook');
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'ss_social_media_facebook', array(
		'label'		=> 'Facebook',
		'section'	=> 'ss_social_media_section',
		'settings'	=> 'ss_social_media_facebook',
	)));

	$wp_customize->add_setting('ss_social_media_linkedin');
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'ss_social_media_linkedin', array(
		'label'		=> 'LinkedIn',
		'section'	=> 'ss_social_media_section',
		'settings'	=> 'ss_social_media_linkedin',
	)));

	$wp_customize->add_setting('ss_social_media_googleplus');
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'ss_social_media_googleplus', array(
		'label'		=> 'Google +',
		'section'	=> 'ss_social_media_section',
		'settings'	=> 'ss_social_media_googleplus',
	)));

	$wp_customize->add_setting('ss_social_media_youtube');
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'ss_social_media_youtube', array(
		'label'		=> 'YouTube',
		'section'	=> 'ss_social_media_section',
		'settings'	=> 'ss_social_media_youtube',
	)));
}

add_action('customize_register', 'ss_theme_customizer');

?>