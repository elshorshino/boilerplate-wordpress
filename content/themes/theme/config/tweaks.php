<?php

add_editor_style();

# Site variables
$site_url = get_bloginfo('url');
$template_url = get_bloginfo('template_url');

# Include CSS admin tweaks file
add_action('admin_head', 'ss_admin_css_tweaks');

function ss_admin_css_tweaks() {
	echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/config/admin.css" type="text/css" />';
}

# Include JS admin tweaks file
add_action('admin_head', 'ss_admin_js_tweaks');

function ss_admin_js_tweaks() {
	echo '<script type="text/javascript" src="'.get_template_directory_uri().'/config/admin.js"></script>';
}

add_action('login_head', 'ss_admin_favicon');
add_action('admin_head', 'ss_admin_favicon');

# Add favicon to WP admin
function ss_admin_favicon() {
	echo '<link rel="shortcut icon" href="' . get_stylesheet_directory_uri() . '/assets/build/graphics/favicon.png" />';
}

# Show "Kitchen Sink" by default
function ss_unhide_kitchensink($args) { $args['wordpress_adv_hidden'] = false; return $args; }
add_filter('tiny_mce_before_init', 'ss_unhide_kitchensink');

# Hide WP Generator Tag
remove_action('wp_head', 'wp_generator');

# Disable wp-embed script
add_action('init', function() {
	if (!is_admin()) {
		wp_deregister_script('wp-embed');
	}
});

# Limit users who can access ACF edit area
add_filter('acf/settings/show_admin', 'my_acf_show_admin');

function my_acf_show_admin($show) {
	// provide a list of usernames who can edit custom field definitions here
	$admins = array('tom', 'simon');

	// get the current user
	$current_user = wp_get_current_user();

	return (in_array($current_user->user_login, $admins));
}

# Options Pages
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
	acf_add_options_sub_page('Header');
	acf_add_options_sub_page('Footer');
}

# Featured Images
add_theme_support('post-thumbnails', array('page','post'));

# Image Sizes
update_option( 'thumbnail_size_w', 170 );
update_option( 'thumbnail_size_h', 140);
update_option('large_size_w', 860);
update_option('medium_size_w', 460);
add_image_size( 'featured-size', 1600, 9999 ); // Unlimited

// for hard cropping - make sure you set smaller images in proportion for srcset
// add_image_size( 'featured-size', 1600, 800, array( 'center', 'top' ) );


# Remove emjoi function
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );	
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param    array  $plugins  
 * @return   array             Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

// Move Yoast Meta Box to bottom
function yoasttobottom() {
	return 'low';
}

add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


?>