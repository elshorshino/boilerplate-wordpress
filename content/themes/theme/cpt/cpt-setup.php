<?php
# CPT Folder
define("CPT_FOLDER", get_stylesheet_directory().'/cpt');
define("CPT_FOLDER_URL", get_stylesheet_directory_uri().'/cpt');

# Include Custom Post Types
require("demo/init.php");

# Change "enter title here" text to match post types
function ss_enter_text_title($title) {
	global $post;

	if(!empty($post->post_type)):
		switch($post->post_type):
			case "demo": return "Enter demo name"; break;
			default: return "Enter title here";
		endswitch;
	else:
		return "Enter title here";
	endif;
}

add_filter('enter_title_here', 'ss_enter_text_title');

# Include CSS CPT tweaks file
add_action('admin_head', 'ss_cpt_css_tweaks');

function ss_cpt_css_tweaks() {
	echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/cpt/assets/css/cpt.css" type="text/css" />';
}
?>