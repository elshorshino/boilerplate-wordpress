<?php
add_action('init', 'DemoInit');

function DemoInit() {
	DemoCPT();
}

# Custom Post Type for Demos
function DemoCPT() {
	register_post_type('demo', array(
		'labels' => array(
			'name' => 'Demos',
			'singular_name' => 'Demo',
			'add_new' => 'Add New Demo',
			'add_new_item' => 'Add New Demo',
			'edit' => 'Edit',
			'edit_item' => 'Edit Demo',
			'new_item' => 'New Demo',
			'view' => 'View',
			'view_item' => 'View Demo',
			'search_items' => 'Search Demos',
			'not_found' => 'No Demos found',
			'not_found_in_trash' => 'Couldn&rsquo;t find any Demos matching that term',
		),
		'description' => 'Custom Post Type to help manage Demos',
		'public' => true,
		'show_ui' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'has_archive' => true,
		'hierarchical' => false,
		'taxonomies' => array('demo-category'),
		'supports' => array('title', 'revisions', 'excerpt'),
		'menu_position' => 21,
		'query_var' => true,
		'rewrite' => array(
			'with_front' => false,
			'slug' => 'demo'
		)
	));
}

# Custom Taxonomy
add_action('init', 'DemoTaxonomy');

function DemoTaxonomy() {
	register_taxonomy('demo-category', array('demo'), array(
		'labels' => array(
			'menu_name' => 'Demo Categories',
			'name' => 'Demo Categories',
			'singular_name' => 'Category',
			'search_items' => 'Search Categories',
			'all_items' => 'All Categories',
			'parent_item' => 'Parent Category',
			'parent_item_colon' => 'Parent Category:',
			'edit_item' => 'Edit Category', 
			'update_item' => 'Update Category',
			'add_new_item' => 'Add New Category',
			'new_item_name' => 'New Category Name'
		),
		'hierarchical' => true,
		'show_ui' => true,
		'query_var' => true
	));
}

# Options page for Demo CPT
if(function_exists('acf_add_options_sub_page')):
	acf_add_options_sub_page(array(
		'title' => 'Demo Options',
		'parent' => 'edit.php?post_type=demo',
		'capability' => 'manage_options'
	));
endif;

?>