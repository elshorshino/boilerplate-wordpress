<?php /* Template Name: Home Page */
the_post();
require("inc/header.php"); ?>
	<main class="main full">
		<div class="content">
			<div class="sleeve">
				<?php the_title("<h1>", "</h1>"); ?>
				<?php the_content(); ?>
			</div>
		</div>
	</main><?php
require("inc/footer.php"); ?>
</body>
</html>