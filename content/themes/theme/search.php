<?php
require("inc/header.php");

global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

foreach($query_args as $key => $string) {
	$query_split = explode("=", $string);
	$search_query[$query_split[0]] = urldecode($query_split[1]);
}

$search = new WP_Query($search_query); ?>
	<main class="main full" role="main">
		<div class="content">
			<div class="sleeve">
				<h1><?php _e("Search results", "Search Results Heading"); ?></h1>
				<p class="subtitle"><?php echo $search->found_posts; ?> result<?php if($search->found_posts != 1): echo 's'; endif; ?> found for &ldquo;<?php echo $_GET["s"]; ?>&rdquo;.</p>
				<ul class="the-results"><?php
					foreach($search->posts as $post): setup_postdata($post); ?>
						<li>
							<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							<div class="text">
								<?php the_excerpt(); ?>
							</div>
						</li><?php
					endforeach; ?>
				</ul><?php
				$prev_posts = get_previous_posts_link("<span>Previous</span>");
				$next_posts = get_next_posts_link("<span>Next</span>");

				$both_options = (isset($prev_posts) && isset($next_posts)) ? true : false;

				if(isset($prev_posts) || isset($next_posts)): ?>
					<ul class="paging<?php if($both_options) echo ' both'; ?>"><?php
						if(isset($prev_posts) && !empty($prev_posts)): ?>
							<li class="prev"><?php echo $prev_posts; ?></li><?php
						endif;

						if(isset($next_posts) && !empty($next_posts)): ?>
							<li class="next"><?php echo $next_posts; ?></li><?php
						endif; ?>
					</ul><?php
				endif; ?>
			</div>
		</div>
	</main><?php
	require("inc/footer.php"); ?>
</body>
</html>