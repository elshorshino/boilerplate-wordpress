
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/assets/lib/jquery.min.js">\x3C/script>')</script>

	<script src="<?php echo get_template_directory_uri(); ?>/assets/build/js/main.min.js"></script><?php

	if(isset($page_js_includes)):
		foreach($page_js_includes as $file_name): ?>
			<script src="<?php echo get_template_directory_uri(); ?>/assets/build/js/<?php echo $file_name; ?>.min.js"></script><?php
		endforeach;
	endif;

	if(isset($admin_js_includes) && current_user_can("publish_pages")):
		foreach($admin_js_includes as $file_name): ?>
			<script src="<?php echo get_template_directory_uri(); ?>/assets/build/js/<?php echo $file_name; ?>.min.js"></script><?php
		endforeach;
	endif;

	wp_footer(); ?>