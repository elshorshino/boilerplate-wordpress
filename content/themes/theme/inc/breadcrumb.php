<?php
# Grab array of parents/ancestors and reverse it, comes out in the wrong order (if exists, if not - don't worry)
$parents = array_reverse(get_post_ancestors($post->ID)); ?>
<div class="crumbtrail">
	<ul><?php
		# If special case/post type, output required crumbs
		switch($post->post_type):
			case "posttype":
				?><li><a href="<?php echo get_post_type_archive_link($post->post_type); ?>"><?php echo get_the_title($post_id); ?></a></li><?php
			break;
		endswitch;

		# If any ancestors, list them out
		if(sizeof($parents) > 0):
			foreach($parents as $post_id): ?><li><a href="<?php echo get_permalink($post_id); ?>"><?php echo get_the_title($post_id); ?></a></li><?php endforeach;
		endif;

		# Output current page/post
		if(is_single() || is_page()):
			?><li><?php the_title(); ?></li><?php
		endif; ?>
	</ul>
</div>