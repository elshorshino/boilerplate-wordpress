<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/build/css/main.min.css" /><?php

	if(isset($admin_css_includes) && current_user_can("publish_pages")):
		foreach($admin_css_includes as $file_name): ?><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/build/css/<?php echo $file_name; ?>" /><?php endforeach;
	endif; ?>

	<title><?php wp_title(' | ', true, 'right'); bloginfo('name'); ?></title>
	<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/build/graphics/favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

	<?php wp_head(); ?>

	<!--[if lt IE 9]><script src="<?php echo get_template_directory_uri(); ?>/assets/lib/html5shiv.min.js"></script><![endif]-->
</head><?php
if(!isset($body_classes)) $body_classes = array(); 
if(!is_404()) $body_classes[] = $post->post_name; ?>
<body class="<?php echo implode($body_classes, " "); ?>">
	<header class="mast-head full">
		<div class="sleeve">
			<div class="logo">
				<a href="/">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/build/graphics/logo.png" alt="<?php bloginfo('name'); ?>" />
				</a>
			</div>
			<nav class="main-nav"><?php
				# Output the main navigation
				echo ss_trans_nav("main_menu"); ?>
			</nav>
		</div>
	</header>
