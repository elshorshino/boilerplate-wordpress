<div id="mast-search">
	<?php $search = get_search_query(); ?>
	<form action="<?php bloginfo('url'); ?>" method="get" class="formbox">
		<fieldset>
			<label for="s">Search</label>
			<input type="search" value="<?php echo $search ?>" class="text placeholder" name="s" id="s" accesskey="4" placeholder="Search" />
			<input type="submit" value="Go" id="search-btn" class="button" />
		</fieldset>
	</form>
</div>